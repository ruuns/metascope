structure Finalizable = MLton.Finalizable
structure Pointer = MLton.Pointer

signature ReadlineIF = sig
  datatype line = Line of string | Eof
  val read : string -> line
end

structure Readline : ReadlineIF = struct
  structure FFI = struct
    val readline = _import "readline" : string -> Pointer.t;
    val strlen = _import "strlen" : Pointer.t -> int;
    val strncpy = _import "strncpy" : char Array.array * Pointer.t * int -> unit;
    val free = _import "free" : Pointer.t -> unit;
  end

  fun copy pointer = 
    let 
      val size = FFI.strlen pointer 
      val buffer = Array.array (size, #"\000")
    in
      FFI.strncpy (buffer, pointer, size);
      Array.vector buffer
    end

  datatype line = Line of string | Eof

  fun read prompt =
    let 
      val p = FFI.readline (prompt ^ "\000")
      val line = if Pointer.null = p then Eof else Line (copy p)
    in
      FFI.free p;
      line
    end
end

structure Repl = struct
  datatype Command =
    | Command of (string * string)
    | Value of string

  fun takeWhile pred str = 
    case Vector.findi (fn (i, x) => pred x) str of
      | NONE        => (str, "")
      | SOME (i, _) => (String.substring (str, 0, i), String.extract (str, i + 1, NONE))

  fun splitCommand str =
    if String.sub (str, 0) = #":" 
      then Command (takeWhile Char.isSpace str)
      else Value str

  fun start f s =
    case Readline.read "meta> " of
      | Readline.Eof => NONE
      | Readline.Line str => Option.mapPartial (start f) (f (splitCommand str, s))
end
