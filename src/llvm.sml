structure Finalizable = MLton.Finalizable
structure Pointer = MLton.Pointer

structure LLVM = struct

structure Context : LLVM_CONTEXT = struct
  type t = Pointer.t Finalizable.t

  val llvmContextCreate = 
    _import "LLVMContextCreate" : unit -> Pointer.t;

  val llvmContextDispose = 
    _import "LLVMContextDispose" : Pointer.t -> unit;

  fun create () = 
    let val v = Finalizable.new (llvmContextCreate ()) 
        val _ = Finalizable.addFinalizer (v, llvmContextDispose);
    in v end
end

structure Types : LLVM_TYPES = struct
  structure Context = Context
  type type_t = Pointer.t

  val llvmDoubleTypeInContext =
    _import "LLVMDoubleTypeInContext" : Pointer.t -> type_t;

  val llvmFunctionType =
    _import "LLVMFunctionType" : type_t * type_t vector * int * int -> type_t;

  fun getDoubleTypeInContext ctx = 
    Finalizable.withValue (ctx, llvmDoubleTypeInContext)

  fun getFunctionType (retTy, tys) =
    llvmFunctionType (retTy, Vector.fromList tys, List.length tys, 0)

end

structure Values : LLVM_VALUES = struct
  structure Types = Types
  type value_t = Pointer.t

  val llvmConstReal =
    _import "LLVMConstReal" : Types.type_t * Real64.real -> value_t;

  val llvmDumpValue = 
    _import "LLVMDumpValue" : value_t -> unit;

  val llvmDeleteFunction =
    _import "LLVMDeleteFunction" : Pointer.t -> unit;

  val llvmCountParams =
    _import "LLVMCountParams" : Pointer.t -> int;

  val llvmGetParams =
    _import "LLVMGetParams" : Pointer.t * Pointer.t array -> unit;

  val llvmSetValueName2 =
    _import "LLVMSetValueName2" : Pointer.t * string * int -> unit;

  val getConstReal = llvmConstReal;
  val dump = llvmDumpValue;
  val deleteFunction = llvmDeleteFunction;
  val countParams = llvmCountParams
  val setValueName2 = llvmSetValueName2

  fun getParams f = 
    let val num = llvmCountParams f
        val v = Array.tabulate (num, fn _ => Pointer.null)
        val () = llvmGetParams (f, v)
    in
      Vector.tabulate (num, fn i => Array.sub (v, i))
    end

end

structure Module : LLVM_MODULE = struct
  structure Context = Context
  structure Values = Values
  structure Types = Types
  type t = Pointer.t Finalizable.t

  val llvmModuleCreateWithNameInContext = 
    _import "LLVMModuleCreateWithNameInContext" : string * Pointer.t -> Pointer.t;

  val llvmDisposeModule = 
    _import "LLVMDisposeModule" : Pointer.t -> unit;

  val llvmDumpModule = 
    _import "LLVMDumpModule" : Pointer.t -> unit;

  val llvmGetNamedFunction = 
    _import "LLVMGetNamedFunction" : Pointer.t * string -> Pointer.t;

  val llvmAddFunction =
    _import "LLVMAddFunction" : Pointer.t * string * Pointer.t -> Pointer.t;

  fun createWithContext (name, ctx) =
    let val m = Finalizable.new (Finalizable.withValue (ctx, fn p => llvmModuleCreateWithNameInContext (name, p)))
        val _ = Finalizable.addFinalizer (m, llvmDisposeModule)
        val _ = Finalizable.finalizeBefore (m, ctx)
    in m end

  fun dump module = 
    Finalizable.withValue (module, llvmDumpModule)

  fun getNamedFunction (module, id) =
    let 
      val r = Finalizable.withValue (module, fn p => llvmGetNamedFunction (p, id))
    in
      if r = Pointer.null 
        then NONE
        else SOME r
    end

  fun addFunction (module, id, funTy) = 
    Finalizable.withValue (module, fn p => llvmAddFunction (p, id, funTy))

end


structure Builder : LLVM_BUILDER = struct
  structure Context = Context
  structure Values = Values
  structure Types = Types
  type t = Pointer.t Finalizable.t

  val llvmCreateBuilderInContext = 
    _import "LLVMCreateBuilderInContext" : Pointer.t -> Pointer.t;

  val llvmDisposeBuilder =
    _import "LLVMDisposeBuilder" : Pointer.t -> unit;

  val llvmBuildFAdd =
    _import "LLVMBuildFAdd" : Pointer.t * Pointer.t * Pointer.t * string -> Pointer.t;

  val llvmBuildFSub =
    _import "LLVMBuildFSub" : Pointer.t * Pointer.t * Pointer.t * string -> Pointer.t;

  val llvmBuildFMul =
    _import "LLVMBuildFMul" : Pointer.t * Pointer.t * Pointer.t * string -> Pointer.t;

  val llvmBuildFDiv =
    _import "LLVMBuildFDiv" : Pointer.t * Pointer.t * Pointer.t * string -> Pointer.t;

  val llvmBuildUIToFP =
    _import "LLVMBuildUIToFP" : Pointer.t * Pointer.t * Pointer.t * string -> Pointer.t;

  val llvmBuildCall =
    _import "LLVMBuildCall" : Pointer.t * Pointer.t * Pointer.t vector * int * string -> Pointer.t;
  
  val llvmBuildRet =
    _import "LLVMBuildRet" : Pointer.t * Pointer.t -> Pointer.t;

  structure RealPredicate : LLVM_REAL_PREDICATE = struct
    type t = int
    val ult = 12
  end

  val llvmBuildFCmp =
    _import "LLVMBuildFCmp" : Pointer.t * RealPredicate.t * Pointer.t * Pointer.t * string -> Pointer.t;

  fun createInContext ctx = 
     let val m = Finalizable.new (Finalizable.withValue (ctx, fn p => llvmCreateBuilderInContext p))
         val _ = Finalizable.addFinalizer (m, llvmDisposeBuilder)
         val _ = Finalizable.finalizeBefore (m, ctx)
      in m end

  fun fadd (builder, lhs, rhs, name) = 
    Finalizable.withValue (builder, fn b => llvmBuildFAdd (b, lhs, rhs, name))

  fun fsub (builder, lhs, rhs, name) = 
    Finalizable.withValue (builder, fn b => llvmBuildFSub (b, lhs, rhs, name))

  fun fmul (builder, lhs, rhs, name) = 
    Finalizable.withValue (builder, fn b => llvmBuildFMul (b, lhs, rhs, name))

  fun fcmp (builder, op', lhs, rhs, name) = 
    Finalizable.withValue (builder, fn b => llvmBuildFCmp (b, op', lhs, rhs, name))

  fun uitofp (builder, i, ty, name) = 
    Finalizable.withValue (builder, fn b => llvmBuildUIToFP (b, i, ty, name))

  fun call (builder, f, args, name) = 
    let 
      val v = Vector.fromList args
      val num = Vector.length v
    in
      Finalizable.withValue (builder, fn b => llvmBuildCall (b, f, v, num, name))
    end

  fun ret (builder, v) =
    Finalizable.withValue (builder, fn p => llvmBuildRet (p, v))
end

structure BasicBlock : LLVM_BASICBLOCK = struct
  structure Context = Context
  structure Values = Values
  structure Builder = Builder
  type t = Pointer.t
  
  val llvmAppendBasicBlock =
    _import "LLVMAppendBasicBlock" : Pointer.t * Pointer.t * string -> Pointer.t;

  fun appendInContext (ctx, f, name) = 
    Finalizable.withValue (ctx, fn p => llvmAppendBasicBlock (p, f, name))
end

structure Instruction : LLVM_INSTRUCTION = struct
  structure Builder = Builder
  structure BasicBlock = BasicBlock

  val llvmGetNumArgOperands = 
    _import "LLVMGetNumArgOperands" : Pointer.t -> int;

  val llvmPositionBuilderAtEnd =
    _import "LLVMPositionBuilderAtEnd" : Pointer.t * Pointer.t -> unit;

  fun getNumArgOperands v = 
    llvmGetNumArgOperands v

  fun positionBuilderAtEnd (builder, block) =
    Finalizable.withValue (builder, fn p => llvmPositionBuilderAtEnd (p, block))
end


end (* structure LLVM *)

