Metascope - the SML variant for implementing a language with LLVM
=================================================================

This project is my personal research playground to come familar with LLVM compiler framework. 
This repository is an implementation to the Kaleidoscope tutorial with a demo language more 
familar to the traditional ML family. I'm aware of LLVM's strong support for OCaml bindings 
which raises a good question why i choosed Standard ML for this project. To cut off the discussion:
It's just my personal choice because i favour the simplicity impression of SML. Another small reason
is my interested to write a bootstrapping compiler someday in the future.


