signature LLVM_CONTEXT = sig
  type t
  val create : unit -> t
end

signature LLVM_TYPES = sig
  structure Context : LLVM_CONTEXT
  type type_t
  val getDoubleTypeInContext : Context.t -> type_t
  val getFunctionType : type_t * type_t list -> type_t
end

signature LLVM_VALUES = sig
  structure Types : LLVM_TYPES

  type value_t
  val getConstReal : Types.type_t * Real64.real -> value_t
  val getParams : value_t -> value_t vector
  val countParams : value_t -> int;
  val dump : value_t -> unit
  val deleteFunction : value_t -> unit;
  val setValueName2 : value_t * string * int -> unit;
end


signature LLVM_MODULE = sig
  structure Context : LLVM_CONTEXT
  structure Values : LLVM_VALUES
  structure Types : LLVM_TYPES
  type t
  val createWithContext : string * Context.t -> t
  val getNamedFunction : t * string -> Values.value_t option
  val addFunction : t * string * Types.type_t -> Values.value_t
  val dump : t -> unit
end

signature LLVM_REAL_PREDICATE = sig
  type t
  val ult : t
end

signature LLVM_BUILDER = sig
  structure Types : LLVM_TYPES
  structure Values : LLVM_VALUES
  structure Context : LLVM_CONTEXT
  structure RealPredicate : LLVM_REAL_PREDICATE
  type t
  
  val createInContext : Context.t -> t
  val fadd : t * Values.value_t * Values.value_t * string -> Values.value_t
  val fsub : t * Values.value_t * Values.value_t * string -> Values.value_t
  val fmul : t * Values.value_t * Values.value_t * string -> Values.value_t
  val fcmp : t * RealPredicate.t * Values.value_t * Values.value_t * string -> Values.value_t
  val uitofp : t * Values.value_t * Types.type_t * string -> Values.value_t
  val call : t * Values.value_t * Values.value_t list * string -> Values.value_t
  val ret : t * Values.value_t -> Values.value_t
end


signature LLVM_BASICBLOCK = sig
  structure Builder : LLVM_BUILDER
  structure Context : LLVM_CONTEXT
  structure Values : LLVM_VALUES
  type t

  val appendInContext : Context.t * Values.value_t * string -> t;
end

signature LLVM_INSTRUCTION = sig
  structure Builder : LLVM_BUILDER
  structure BasicBlock : LLVM_BASICBLOCK

  val positionBuilderAtEnd : Builder.t * BasicBlock.t -> unit;
end
