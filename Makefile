.PHONY: clean build
OUTDIR=targets
TARGETS=${OUTDIR} ${OUTDIR}/metascope

SCANNER=src/scanner.lex
PARSER=src/parser.grm

MLFLAGS=-verbose 0 -default-ann 'allowSuccessorML true'
LLVMFLAGS=$(shell llvm-config --ldflags --libs core --system-libs)

build: ${TARGETS}

clean:
	rm -rf ${TARGETS}
	rm -rf ${SCANNER}.sml
	rm -rf ${PARSER}.sml
	rm -rf ${PARSER}.sig

${OUTDIR}:
	mkdir -p ${OUTDIR}

${SCANNER}.sml: ${SCANNER}
	mllex $^

${PARSER}.sml: ${PARSER}
	mlyacc $^

${OUTDIR}/metascope: metascope.mlb ${SCANNER}.sml ${PARSER}.sml
	mlton -output $@ ${MLFLAGS} -link-opt -lreadline -link-opt "${LLVMFLAGS}" $<
