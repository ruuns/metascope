structure Metascope = struct
  datatype expr = 
    | Number of Real.real
    | Var of string
    | Binary of string * expr * expr
    | App of string * expr list

  datatype function = 
    Function of string * string list * expr

  datatype foreign_prototyp =
    ForeignPrototyp of string * string list

  datatype toplevel = 
    | Expr of expr 
    | Foreign of foreign_prototyp
    | Fun of function
end
