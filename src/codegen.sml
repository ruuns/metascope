structure M = Metascope

structure Codegen = struct
  structure Map = RedBlackMapFn(struct type ord_key = String.string; open String end)
  exception Error of string

  val globalContext = LLVM.Context.create ()
  val globalModule = LLVM.Module.createWithContext ("interpreter", globalContext)
  val globalBuilder = LLVM.Builder.createInContext globalContext
  val typeDouble = LLVM.Types.getDoubleTypeInContext globalContext

  val emptyEnv = Map.empty

  fun translateExpr (env, M.Number x) = 
      LLVM.Values.getConstReal (typeDouble, x)

    | translateExpr (env, M.Var id) =
      (Map.lookup (env, id) handle | NotFound => raise Error ("Unknown variable " ^ id))
      
    | translateExpr (env, M.Binary (op', e1, e2)) = 
      let 
        val lhs = translateExpr (env, e1)
        val rhs = translateExpr (env, e2)
      in 
        case op' of 
          | "+" => LLVM.Builder.fadd (globalBuilder, lhs, rhs, "addtmp")
          | "-" => LLVM.Builder.fsub (globalBuilder, lhs, rhs, "subtmp")
          | "*" => LLVM.Builder.fmul (globalBuilder, lhs, rhs, "multmp")
          | "<" => let val i = LLVM.Builder.fcmp (globalBuilder, LLVM.Builder.RealPredicate.ult, lhs, rhs, "cmptmp") 
                    in LLVM.Builder.uitofp (globalBuilder, i, typeDouble, "booltmp") end
          | _   => raise Error ("Unknown binary operator " ^ op')
      end

    | translateExpr (env, M.App (id, es)) =
      let 
        val callee =
          case LLVM.Module.getNamedFunction (globalModule, id) of
            | SOME c  => c
            | NONE    => raise Error ("Unknown function " ^ id ^ " referenced")
        val numArgs = LLVM.Values.countParams callee
       in 
         if numArgs <> List.length es 
           then raise Error ("Incorrect number of arguments passed")
           else LLVM.Builder.call (globalBuilder, callee, List.map (fn e => translateExpr (env, e)) es, "calltmp")
      end


  fun translateForeign (env, M.ForeignPrototyp (name, args)) =
    let 
      val doubles = List.map (fn _ => typeDouble) args
      val funty = LLVM.Types.getFunctionType (typeDouble, doubles)
    in
      case LLVM.Module.getNamedFunction (globalModule, name) of
        | NONE   => LLVM.Module.addFunction (globalModule, name, funty)
        | SOME _ => raise Error ("Redefinition of function " ^ name)
    end


  fun translateFunction (env, M.Function (name, args, body)) =
    let 
      val doubles = List.map (fn _ => typeDouble) args
      val funty = LLVM.Types.getFunctionType (typeDouble, doubles)
      val f = 
        case LLVM.Module.getNamedFunction (globalModule, name) of
          | NONE   => LLVM.Module.addFunction (globalModule, name, funty)
          | SOME _ => raise Error ("Redefinition of function " ^ name)
      val names = Vector.fromList args
      val fargs = LLVM.Values.getParams f
      fun setArg (i, arg) = LLVM.Values.setValueName2 (Vector.sub (fargs, i), arg, String.size arg)
      val () = Vector.appi setArg names
      val block = LLVM.BasicBlock.appendInContext (globalContext, f, "entry")
      val () = LLVM.Instruction.positionBuilderAtEnd (globalBuilder, block)
      val env' = env
    in 
      let val expr = translateExpr (env', body)
          val _ = LLVM.Builder.ret (globalBuilder, expr)
        in f end 
          handle e => (LLVM.Values.deleteFunction f; raise e)
    end
 
end
