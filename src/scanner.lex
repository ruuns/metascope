structure T = Tokens

type pos = int
type svalue = T.svalue
type ('a, 'b) token = ('a, 'b) T.token
type lexresult = (svalue, pos) token
type lexarg = string
type arg = lexarg

val yyline = ref 1
val yyoffset = ref 0

val eof = fn _ => T.EOF (0, 0)

val filterUnderscore = 
  String.implode o List.filter (fn x => x <> #"_") o String.explode

fun parseInt radix (str, line) =
  case StringCvt.scanString (Int.scan radix) (filterUnderscore str) of
    | SOME x => T.FLOAT (Real.fromInt x, line, line)
    | NONE   => T.ERROR ("Could not scan integer literal '" ^ str ^ "'", line, line)

fun parseFloat (str, line) = 
  case StringCvt.scanString Real.scan (filterUnderscore str) of
    | SOME x => T.FLOAT (x, line, line)
    | NONE   => T.ERROR ("Could not scan float literal '" ^ str ^ "'", line, line)

fun token (parser, str, line) =
  parser (str, line) 

fun loc off =
  (!yyline, !yyline)

%%
%full
%header (functor MetascopeLexFun(structure Tokens: Metascope_TOKENS));
%arg (filename:string);

decdigit=[0-9];
decnumber={decdigit}("_"|{decdigit})*;
frac="."{decnumber};
exp=[eE](-?){decnumber};
real=(~?)(({decnumber}{frac}?{exp})|({decnumber}{frac}{exp}?));

ws=\t|"\011"|"\012"|" ";
cr="\013";
nl="\010";
newline=({cr}{nl}|{cr}|{nl});

alphanum=[A-Za-z0-9'_];
varId=[A-Za-z_]{alphanum}*;

%%

<INITIAL>{newline}   => (yyline := !yyline + 1; yyoffset := yypos; continue ());
<INITIAL>{ws}+       => (continue ());
<INITIAL>{decnumber} => (token (parseInt StringCvt.DEC, yytext, !yyline));
<INITIAL>{real}      => (token (parseFloat, yytext, !yyline));
<INITIAL>"fun"       => (T.FUN (loc yypos));
<INITIAL>"foreign"   => (T.FOREIGN (loc yypos));
<INITIAL>";"         => (T.SEMICOLON (loc yypos));
<INITIAL>"("         => (T.LB (loc yypos));
<INITIAL>")"         => (T.RB (loc yypos));
<INITIAL>"->"        => (T.ARROW (loc yypos));
<INITIAL>":"         => (T.COLON (loc yypos));
<INITIAL>"="         => (T.EQ (loc yypos));
<INITIAL>"+"         => (T.OPADD ("+", !yyline, !yyline));
<INITIAL>"-"         => (T.OPADD ("-", !yyline, !yyline));
<INITIAL>"*"         => (T.OPMULT ("*", !yyline, !yyline));
<INITIAL>"<"         => (T.OPREL ("<", !yyline, !yyline));
<INITIAL>"<="        => (T.OPREL ("<=", !yyline, !yyline));
<INITIAL>">="        => (T.OPREL (">=", !yyline, !yyline));
<INITIAL>">"         => (T.OPREL (">", !yyline, !yyline));
<INITIAL>"=="        => (T.OPEQUAL ("==", !yyline, !yyline));
<INITIAL>"<>"        => (T.OPEQUAL ("<>", !yyline, !yyline));
<INITIAL>{varId}     => (T.IDENT (yytext, !yyline, !yyline));
<INITIAL>.           => (T.ERROR (yytext, !yyline, !yyline));
