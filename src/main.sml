structure Process = OS.Process
structure M = Metascope

fun interprete (str, s) = 
  let val (ast, _) = Parser.parse (TextIO.openString str, "<interactive>") in
    case ast of
      | M.Expr e :: _    => (LLVM.Values.dump (Codegen.translateExpr (Codegen.emptyEnv, e)); print "\n")
      | M.Fun f :: _     => (LLVM.Values.dump (Codegen.translateFunction(Codegen.emptyEnv, f)); print "\n")
      | M.Foreign f :: _ => (LLVM.Values.dump (Codegen.translateForeign (Codegen.emptyEnv, f)); print "\n")
      | []               => ();
    s
  end


fun process (Repl.Command (":quit", _), _) = 
    NONE

  | process (Repl.Command (":mod", _), s) = 
    (LLVM.Module.dump Codegen.globalModule; print "\n"; SOME s)

  | process (Repl.Command (cmd, _), s) = 
    (print (cmd ^ " currently not implemented\n"); SOME s)

  | process (Repl.Value str, s) = 
    let val s' = interprete (str, s) handle ex => (print (General.exnMessage ex ^ "\n"); s) in
      SOME s'
    end


fun run prog _ = 
  (Repl.start process (); print "Cya\n")


fun main () =
  let
    val prog = CommandLine.name ()
    val args = CommandLine.arguments ()
  in 
    run prog args
      handle ex => print (General.exnMessage ex ^ "\n");
    Process.exit Process.failure
  end

val _ = main ();
