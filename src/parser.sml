structure Parser = struct
  structure LrVals = MetascopeLrValsFun(structure Token = LrParser.Token)
  structure Lex = MetascopeLexFun(structure Tokens = LrVals.Tokens)
  structure Yacc = JoinWithArg(
    structure ParserData = LrVals.ParserData
    structure Lex = Lex
    structure LrParser = LrParser)

  fun grab stream =
    fn n =>
      if TextIO.endOfStream stream 
        then ""
        else TextIO.inputN (stream, n)

  fun printError (msg, line, column) = 
    print ("ParserError: " ^ msg ^ "\n")

  fun parse (input, filename) =
    let 
      val look = 15
      val lexer = Yacc.makeLexer (grab input) filename
    in
      Yacc.parse (look, lexer, printError, filename)
    end
end
